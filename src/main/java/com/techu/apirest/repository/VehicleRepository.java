package com.techu.apirest.repository;

import com.techu.apirest.model.ClientModel;
import com.techu.apirest.model.VehicleModel;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface VehicleRepository extends MongoRepository<VehicleModel, String> {

}
