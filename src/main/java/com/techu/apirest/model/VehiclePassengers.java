package com.techu.apirest.model;

import java.util.ArrayList;

public class VehiclePassengers {

    private String idVehicle;
    private ArrayList<ClientModel> passengers;

    public VehiclePassengers(String idVehicle, ArrayList<ClientModel> passengers) {
        this.idVehicle = idVehicle;
        this.passengers = passengers;
    }

    public String getIdVehicle() {
        return idVehicle;
    }

    public void setIdVehicle(String idVehicle) {
        this.idVehicle = idVehicle;
    }

    public ArrayList<ClientModel> getPassengers() {
        return passengers;
    }

    public void setPassengers(ArrayList<ClientModel> passengers) {
        this.passengers = passengers;
    }
}
