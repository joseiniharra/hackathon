package com.techu.apirest.model;

import org.jetbrains.annotations.NotNull;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.lang.reflect.Array;
import java.util.ArrayList;

@Document(collection = "vehiculos")
public class VehicleModel {

    @Id
    @NotNull
    private String idVehicle;
    private Integer freeSeats;
    private String plate;
    private String model;
    private String colour;
    private String origin;
    private String destination;
    private ArrayList<ClientModel> passengers;
    private Boolean disponible;

    public VehicleModel(){

    }
    public VehicleModel(@NotNull String idVehicle, Integer freeSeats, String plate, String model, String colour, String origin, String destination, ArrayList<ClientModel> passengers, Boolean disponible) {
        this.idVehicle = idVehicle;
        this.freeSeats = freeSeats;
        this.plate = plate;
        this.model = model;
        this.colour = colour;
        this.origin = origin;
        this.destination = destination;
        this.passengers = passengers;
        this.disponible = disponible;
    }

    public String getIdVehicle() {
        return idVehicle;
    }

    public void setIdVehicle(String idVehicle) {
        this.idVehicle = idVehicle;
    }

    public Integer getFreeSeats() {
        return freeSeats;
    }

    public void setFreeSeats(Integer freeSeats) {
        this.freeSeats = freeSeats;
    }

    public String getPlate() {
        return plate;
    }

    public void setPlate(String plate) {
        this.plate = plate;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getColour() {
        return colour;
    }

    public void setColour(String colour) {
        this.colour = colour;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public ArrayList<ClientModel> getPassengers() {
        return passengers;
    }

    public void setPassengers(ArrayList<ClientModel> passengers) {
        this.passengers = passengers;
    }

    public Boolean getDisponible() {
        return disponible;
    }

    public void setDisponible(Boolean disponible) {
        this.disponible = disponible;
    }
}