package com.techu.apirest.service;

import com.techu.apirest.model.ClientModel;
import com.techu.apirest.repository.ClientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ClientService {

    //con el autowired no es necesrio hacer un new. Cuando lo usemos, spring lo crea solito
    @Autowired
    ClientRepository clientRepository;

    //READ
    public List<ClientModel> findAll(){
        return clientRepository.findAll();
    }

    //READ BY ID
    //optional --> si una clase no devuelve el tipo de dato que espera el método, que lo trate igualmente...
    // es porque el findid igual no encuentra producto con ese ID, ojo...
    public Optional<ClientModel> findById(String id){
        return clientRepository.findById(id);
    }



    //CREATE
    public ClientModel save(ClientModel cliente){
        return clientRepository.save(cliente);
    }

    //DELETE
    public boolean delete(ClientModel cliente){
        try {
            clientRepository.delete(cliente);
            return true;
        }catch(Exception e){
            return false;
        }
        //habría que controlar bien esto, ya que siempre da true, porque el try solo casca si hay algún
        //problema con el delete. Si intentamos borrar uno no existente, no pasa nada, no casca, así que da true igual
    }

    //DELETEBYID
    public void deleteById(String id){
        clientRepository.deleteById(id);
    }

    //existsby ID
    public boolean existsById(String id){
        return(clientRepository.existsById(id));
    }

}
