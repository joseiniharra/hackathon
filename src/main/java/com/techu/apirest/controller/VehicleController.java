package com.techu.apirest.controller;


import com.techu.apirest.model.ClientModel;
import com.techu.apirest.model.VehicleModel;
import com.techu.apirest.model.VehiclePassengers;
import com.techu.apirest.service.ClientService;
import com.techu.apirest.service.VehicleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@CrossOrigin(origins = "*", methods = {RequestMethod.GET, RequestMethod.POST})
@RequestMapping("/apivehicles/v1")
public class VehicleController {

    @Autowired
    VehicleService vehicleService;

    @Autowired
    ClientService clientService;

// MÉTODOS PARA VEHICULOS

    @GetMapping("/vehicles")
    public ResponseEntity<List<VehicleModel>> getVehicleOrigen(@RequestParam (value="origin" , defaultValue = "todos") String origen, @RequestParam (value="destination" ,defaultValue = "todos") String destino){
        List<VehicleModel> lista =  vehicleService.findAll();
        ArrayList<VehicleModel> listaResultado = new ArrayList<VehicleModel>();
        VehicleModel vehiculoItera;

        if (origen.equals("todos") && destino.equals("todos")){
            return new ResponseEntity<>(vehicleService.findAll(),HttpStatus.OK);
        }

        for (int i=0;i<lista.size();i++){

            vehiculoItera = lista.get(i);

            if(vehiculoItera.getFreeSeats()>vehiculoItera.getPassengers().size()) {

            /*System.out.println("iteracion "+ i);
            System.out.println(" numero registros "+ lista.size());
            System.out.println("vehiculo itera origen "+ vehiculoItera.getOrigin() + " - "+origen);
            System.out.println("vehiculo itera destino "+ vehiculoItera.getDestination() + " - "+destino);
*/
                if (origen.equals("todos") && vehiculoItera.getDestination().equals(destino)) {
                    listaResultado.add(vehiculoItera);
                }
                if (vehiculoItera.getOrigin().equals(origen) && destino.equals("todos")) {
                    listaResultado.add(vehiculoItera);
                }
                if (vehiculoItera.getOrigin().equals(origen) && vehiculoItera.getDestination().equals(destino)) {
                    listaResultado.add(vehiculoItera);

                }
            }
        }
        if (listaResultado.isEmpty()){
            ArrayList<VehicleModel> noProducto = new ArrayList<VehicleModel>();
            return new ResponseEntity<>(noProducto,HttpStatus.NO_CONTENT);
        }else{
            return new ResponseEntity<>(listaResultado,HttpStatus.OK);
        }

    }


    @GetMapping( "/vehicles/clients/{passenger}")
    public ResponseEntity<VehicleModel> getVehicleOrigen(@PathVariable String passenger){

        List<VehicleModel> lista =  vehicleService.findAll();
        VehicleModel vehiculoItera;


        for (int i=0;i<lista.size();i++) {

            vehiculoItera = lista.get(i);
         //   System.out.println("iteración vehículo "+ i + " numero total vehiculos: " + lista.size());
         //   System.out.println("numero total pasajeros: "+vehiculoItera.getPassengers().size() + " Passenger pasado: "+passenger);

            for (int j = 0; j < vehiculoItera.getPassengers().size(); j++) {
                if (vehiculoItera.getPassengers().get(j).getIdClient().equals(passenger)) {
                    // el cliente ya está en un coche metido
           //         System.out.println("iteración passenger "+ j + " numero total pasajeros: "+vehiculoItera.getPassengers().size());
                    return new ResponseEntity<>(vehiculoItera,HttpStatus.FOUND);
                }
            }
        }
        VehicleModel noProducto = new VehicleModel();
        return new ResponseEntity<>(noProducto,HttpStatus.NOT_FOUND);
    }


    @GetMapping("/vehicles/{id}")
    public Optional<VehicleModel> getVehicleId(@PathVariable String id){
        return vehicleService.findById(id);
    }

     //añadir vehiculo nuevo
    @PostMapping( "/vehicles")
    public ResponseEntity<VehicleModel> postVehicles(@RequestBody VehicleModel newVehicle){
        VehicleModel vehiclenull = new VehicleModel();
        if(vehicleService.existsById(newVehicle.getIdVehicle())){
            //Ya existe el producto, no lo puedo insertar...
            return new ResponseEntity<>(vehiclenull, HttpStatus.IM_USED);
        }else{
            //no existe, lo puedo crear:
            vehicleService.save(newVehicle);
            return new ResponseEntity<>(newVehicle, HttpStatus.CREATED);
        }
    }

    //actualizar producto existente. Esta vez no pasamos el id, lo cogeremos del producto directamente
    @PutMapping( "/vehicles")
    public ResponseEntity<VehicleModel> putVehicles(@RequestBody VehicleModel vehicleToUpdate){
        VehicleModel vehiclenull = new VehicleModel();
        if (vehicleService.existsById(vehicleToUpdate.getIdVehicle()) ){
            // Ya existe el producto, por lo que lo puedo modificar
            vehicleService.save(vehicleToUpdate);
            return new ResponseEntity<>(vehicleToUpdate, HttpStatus.ACCEPTED);//("Producto actualizado correctamente");
        } else {
            // no existe el producto, no lo puedo actualizar
            return new ResponseEntity<>(vehiclenull, HttpStatus.NOT_FOUND);
        }
    }


    // ahora un delete
    @DeleteMapping ( "/vehicles")
    public ResponseEntity<Boolean> delVehicles(@RequestBody VehicleModel vehicleToDelete){
        if(vehicleService.existsById(vehicleToDelete.getIdVehicle())){
            vehicleService.delete(vehicleToDelete);
            return new ResponseEntity<>(true, HttpStatus.OK);
        }else{
            return new ResponseEntity<>(false, HttpStatus.NOT_FOUND);
        }

    }

    @DeleteMapping ( "/vehicles/{id}")
    public ResponseEntity<Boolean> delVehiclesID(@PathVariable String id){
        if (vehicleService.existsById(id)){
            vehicleService.deleteById(id);
            return new ResponseEntity<>(true, HttpStatus.OK);
        }else{
            return new ResponseEntity<>(false, HttpStatus.NOT_FOUND);
        }

    }

    @PatchMapping ("/vehicles")
    public ResponseEntity<VehicleModel> patchVehicle(@RequestBody VehiclePassengers vehiclePassengers){
        Optional <VehicleModel> vehicleRec = vehicleService.findById(vehiclePassengers.getIdVehicle());
        VehicleModel vehicleAux;

        if (vehicleRec.isPresent()){
            vehicleAux = vehicleRec.get();
            vehicleAux.setPassengers(vehiclePassengers.getPassengers());
            vehicleService.save(vehicleAux);
            return new ResponseEntity<>(vehicleAux,HttpStatus.OK);

        }  else {
            vehicleAux = new VehicleModel();
            return new ResponseEntity<>(vehicleAux, HttpStatus.NOT_MODIFIED);
        }
    }


    // MÉTODOS PARA CLIENTES

    @GetMapping("/clients")
    public ResponseEntity<List<ClientModel>> getClients(){
        if (clientService.findAll().isEmpty()){
            ArrayList<ClientModel> noProducto = new ArrayList<ClientModel>();
            return new ResponseEntity<>(noProducto,HttpStatus.NO_CONTENT);
        } else{
            return new ResponseEntity<>(clientService.findAll(),HttpStatus.OK);
        }
    }

//  Consulta elemento por id

    @GetMapping("/clients/{id}")
    public Optional<ClientModel> getClienteId(@PathVariable String id){
        return clientService.findById(id);
    }


    @PostMapping( "/clients")
    public ResponseEntity<ClientModel> postClients(@RequestBody ClientModel newClient){
        ClientModel clientnull = new ClientModel();
        if(clientService.existsById(newClient.getIdClient())){
            //Ya existe el producto, no lo puedo insertar...
            return new ResponseEntity<>(clientnull, HttpStatus.IM_USED);
        }else{
            //no existe, lo puedo crear:
            clientService.save(newClient);
            return new ResponseEntity<>(newClient, HttpStatus.CREATED);
        }
    }

    //actualizar producto existente. Esta vez no pasamos el id, lo cogeremos del producto directamente
    @PutMapping( "/clients")
    public ResponseEntity<ClientModel> putVehicles(@RequestBody ClientModel clientToUpdate){
        ClientModel clientnull = new ClientModel();
        if (clientService.existsById(clientToUpdate.getIdClient()) ){
            // Ya existe el producto, por lo que lo puedo modificar
            clientService.save(clientToUpdate);
            return new ResponseEntity<>(clientToUpdate, HttpStatus.ACCEPTED);//("Producto actualizado correctamente");
        } else {
            // no existe el producto, no lo puedo actualizar
            return new ResponseEntity<>(clientnull, HttpStatus.NOT_FOUND);
        }
    }


    // ahora un delete
    @DeleteMapping ( "/clients")
    public ResponseEntity<Boolean> delClients(@RequestBody ClientModel clientToDelete){
        if(clientService.existsById(clientToDelete.getIdClient())){
            clientService.delete(clientToDelete);
            return new ResponseEntity<>(true, HttpStatus.OK);
        }else{
            return new ResponseEntity<>(false, HttpStatus.NOT_FOUND);
        }

    }

    @DeleteMapping ( "/clients/{id}")
    public ResponseEntity<Boolean> delclientsID(@PathVariable String id){
        if (clientService.existsById(id)){
            clientService.deleteById(id);
            return new ResponseEntity<>(true, HttpStatus.OK);
        }else{
            return new ResponseEntity<>(false, HttpStatus.NOT_FOUND);
        }

    }



}
